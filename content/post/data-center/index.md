---
title: Pengenalan Data Center
description: Penjelasan dan pengenalan mengenai data center
date: 2023-10-30
slug: Data_Center
image: data-center-page.png
categories:
    - Data_Center
tags:
    - Networking
---

## Apa itu Data Center?

Data center adalah fasilitas fisik yang digunakan untuk menyimpan, mengelola, dan menyediakan akses data, serta aplikasi komputer. Data center merupakan tempat di mana sejumlah besar perangkat keras komputer, seperti server, penyimpanan data, perangkat jaringan, dan infrastruktur terkait lainnya, diatur dan dioperasikan. Data center berperan penting dalam menyimpan informasi, memproses data, dan menyediakan layanan komputasi yang krusial bagi organisasi, bisnis, dan pengguna.

Data Center memusatkan konsolidasi sumber daya teknologi informasi, memungkinkan organisasi melaksanakan bisnis sepanjang waktu. Dengan kata lain, Data center merupakan "Hardened facility" yang didedikasikan untuk menyediakan layanan operasi business-critical data.

![HP Data Center](HPdatacenter.jpg)

## Inisiatif Bisnis Menggunakan Data Center

- Konsolidasi server dan pemusatan kapabilitas pemrosesan data
- Sebagai bagian dari rencana bisnis berkelanjutan atau rencana pemulihan dari sebuah bencara
- Manajemen storage dan database
- Mengubah atau mengintegrasikan menjadi layanan website  dari sebuah applikasi bisnis atau biasa disebut dengan "webification"
- Distribusi informasi melalui internet ataupun extranet
- eBusiness dan Electronic Data Interexchange (EDI), EDI merupakan sebuah konsep pertukaran data elektronik antara organisasi bisnis yang dilakukan secara otomatis, biasanya dalam format standar yang telah ditetapkan sebelumnya. 
- Supply Chain Management (SCM) and Enterprise Resource Planning (ERP). SCM merupakan strategi untuk mengatur dan mengelola semua tahapan perjalanan produk, mulai dari pengadaan bahan baku hingga pengiriman ke tangan konsumen akhir. Sedangkan ERP merupakan sistem terintegrasi yang menggabungkan berbagai fungsi bisnis dalam satu database sentral.
- Customer Relationship Management (CRM), CRM adalah strategi yang difokuskan pada hubungan antara perusahaan dengan para pelanggannya, tujuannya untuk meningkatkan retensi pelanggan, memperkuat loyalitas mereka, serta memungkinkan perusahaan untuk memberikan layanan yang lebih baik dan lebih sesuai dengan kebutuhan pelanggan
- Sales Force Automation (SFA), SFA merupakan penggunaan teknologi dan alat-alat otomatis untuk membantu tim penjualan dalam melakukan tugas-tugasnya. contohnya seperti melakukan otomatisasi di bagian manajemen kontak, pelacakan prospek, manajemen jadwal, dan pelaporan penjualan.
- Wireless application and connectivity

## Konsiderasi Infrastruktur Data Center

**Racking**\
Racking bertujuan untuk menyusun perangkat keras seperti server, switch, router, dan perangkat lainnya di dalam data center. 

**Cabling**\
Pengkabelan yang rapi dan terorganisir adalah kunci dalam data center, termasuk kabel yang menghubungkan perangkat-perangkat dan jaringan. Penggunaan kabel yang sesuai, penataan kabel yang rapi, serta pengelolaan kabel yang baik akan mempermudah pemeliharaan dan troubleshooting.

**Security and Monitoring**\
Aspek keamanan contohnya seperti pengawasan fisik (kamera CCTV). Sistem monitoring bertujuan contohnya sebagai pemantauan suhu, kelembaban, serta keamanan untuk mengidentifikasi masalah sejak dini.

**Redundancy and path diversity**\
Redundansi bertujuan untuk memastikan ada backup atau cadangan untuk mencegah kegagalan sistem. Sedangkan path diversity, berkaitan dengan penggunaan jalur komunikasi alternatif untuk memastikan ketersediaan dan kinerja jaringan yang optimal.

**Security**\
Keamanan data center melibatkan beberapa langkah-langkah untuk melindungi informasi dari ancaman fisik dan siber, meliputi penggunaan teknologi keamanan yang ketat.

**Storage**\
Penyimpanan data adalah bagian penting dari infrastruktur data center. Hal ini mencakup pemilihan teknologi penyimpanan yang sesuai dengan kebutuhan serta implementasi kebijakan backup yang efektif.

**Appropriate cabling media**\
Pemilihan media kabel yang tepat, seperti serat optik atau kabel tembaga, merupakan hal penting untuk memastikan kecepatan, keandalan, dan kemampuan jaringan yang optimal.

## Fitur Data Center

**Ketersediaan 7 x 24 x 365**\
Berarti layanan dan akses terhadap data selalu aktif dan tersedia kapan pun diperlukan.

**Fail safe reliability dan monitoring berkelanjutan**\
Sistem di data center didesain untuk dapat beroperasi secara andal dengan pengamanan tambahan untuk mengatasi kegagalan. 

**Manajemen daya atau power dan network communications, redundancy, dan path diversity**\
Sistem data center memperhitungkan manajemen daya yang efisien serta jaringan komunikasi yang handal.

**Keamanan Jaringan, access control fisik dan pengawasan cctv**\
Keamanan sistem jaringan dijaga dengan sangat serius. Ada kontrol akses fisik yang ketat untuk memastikan bahwa hanya orang yang berwenang yang dapat mengakses pusat data tersebut.

**Zoned Environmental Control**\
Data center memiliki kontrol lingkungan yang sangat ketat untuk memastikan suhu dan kelembaban tetap stabil, bertujuan agar hardware di dalam data center beroperasi dalam kondisi optimal.

**Fire suppression and early warning smoke detection systems**\
Sistem pemadaman api dan deteksi asap dipasang untuk menangani kebakaran sesegera mungkin dan meminimalkan kerusakan yang mungkin terjadi pada perangkat keras dan infrastruktur data center.

## Tipe Data Center

### Corporate Data Center (CDC)

CDC bertindak sebagai pusat utama untuk penyimpanan, pengolahan, dan manajemen data serta sistem teknologi informasi sebuah perusahaan. CDC seringkali merupakan "otak" dari operasi teknologi perusahaan, menyediakan tempat bagi server, database, aplikasi, dan infrastruktur IT yang mendasari berbagai layanan dan operasi.

CDC ini berfungsi sebagai jantung dari kegiatan teknologi informasi dalam suatu perusahaan. Segala sesuatu yang terkait dengan data, aplikasi bisnis, dan operasional teknologi terpusat di sini. Keamanan, ketersediaan, dan keandalan data merupakan fokus utama dari CDC. Dengan demikian, seringkali diberlakukan standar keamanan dan sistem redundansi yang kuat untuk melindungi dan menjaga data serta operasi bisnis agar tetap berjalan sebagaimana mestinya.

CDC menjadi landasan penting yang memungkinkan berbagai departemen dan fungsi perusahaan menggunakan sistem IT dengan aman, andal, dan efisien. CDC juga merupakan komponen utama dalam memastikan bahwa teknologi perusahaan beroperasi sebagaimana mestinya untuk mendukung kesuksesan dan produktivitas bisnis.

Karakteristik CDC meliputi:

- Corporate Data Center merupakan suatu data center yang dimiliki oleh satu perusahaan private, institusi, atau agensi pemerintah.
- Perlengkapan, aplikasi, support dan maintenance data center ini biasanya dimanage oleh IT Departemen perusahaan tersebut

### Internet Data Center

Internet Data Center (IDC) merupakan sebuah pusat fisik yang berfungsi sebagai lokasi pusat untuk penyimpanan data, pengelolaan sistem, dan infrastruktur jaringan komputer yang penting untuk layanan online dan operasi perusahaan. IDC adalah tempat di mana server, perangkat keras, dan sistem jaringan disimpan dan dioperasikan untuk menyediakan layanan digital kepada pengguna.

IDC memiliki peran vital dalam menampung informasi penting yang digunakan dalam berbagai layanan online. IDC adalah tempat di mana data, aplikasi, dan layanan internet disimpan dan dielola. Keamanan dan keandalan merupakan aspek penting dari IDC, sehingga seringkali dilengkapi dengan sistem keamanan yang canggih untuk melindungi data dari ancaman eksternal.

Selain menyediakan tempat untuk menyimpan data, IDC juga memastikan ketersediaan dan aksesibilitas data secara terus-menerus, 24 jam sehari, 7 hari seminggu, sepanjang tahun. IDC membutuhkan perawatan dan pemantauan yang terus-menerus untuk memastikan bahwa infrastruktur beroperasi tanpa hambatan.

Lebih dari sekadar gudang data, IDC adalah fondasi dari berbagai layanan digital yang kita gunakan setiap hari, mulai dari aplikasi hingga situs web. Pengelolaan yang cermat, keamanan yang kuat, dan ketersediaan yang terjamin adalah inti dari peran IDC dalam menyediakan layanan online yang handal dan aman bagi pengguna di seluruh dunia.

Karakteristik IDC meliputi:

- Internet Data Center biasanya dimiliki dan dijalankan oleh perusahaan telekomunikasi, penyedia layanan yang tidak teregulasi, atau type operator komersial lainnya.
- Menyediakan pilihan layanan kepada client mereka seperti wide-area communications, internet access, web or application hosting, colocation, managed servers, storage network, content distribution and load sharing dengan variant baru yang terus muncul.
- Sekarang, hampir semuanya dioperasikan oleh penyedia co-location, ataupun penyedia layanan cloud

## Data Center Tiers by ANSI

Tiers atau tingkatan dalam data center dibagi menjadi 4 level, dan setiap level terdapat minimal waktu availability/ketersediaan/uptime dalam satu tahun. Kita bisa menghitungnya di situs website berikut [uptime.is](https://uptime.is)

### Tier Level 1

- Jalur distribusi single non-redundant, melayani perlengkapan IT
- Komponen kapasitas non-redundant
- Infrastructure basic dengan ekspektasi ketersediaan 99.671%

### Tier Level 2

- Mencakup semya persyaratan tier 1
- Kapasitas komponen redundant site infrastructure dengan ekspektasi ketersediaan 99.741%

### Tier Level 3

- Mencakup semua persyaratan tier 2
- Beberapa jalur distribusi independent melayani perlengkapan IT
- Semua perlengkapan IT harus memilikliki minimal dual-powered dan kompatible sepenuhnya dengan topology yang ada
- Dapat di maintain secara bersamaan dengan ekspektasi ketersediaan 99.82%

### Tier Level 4

- Mencakup semua persyaratan tier 3
- Semua peralatan pendinginan harus dual-powered secara independent, termasuk pendingin dan penghangat, ventilasi dan air conditioning (HVAC) systems
- Fault-tolerant site infrastructure dengan penyimpanan daya electrik dan fasilitas distribusi  dengan ekspektasi ketersediaan 99.995%

![](sla-datacenter.png)
