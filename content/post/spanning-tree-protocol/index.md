---
title: Spanning Tree Protocol
description: Penjelasan tentang Spanning Tree Protocol di Cisco
date: 2023-10-30
slug: Cisco
image: stp-page.png
categories:
    - Cisco
tags:
    - Networking
    - Switching
---

## Apa itu STP?

STP (Spanning Tree Protocol) merupakan sebuah protokol yang berfungsi untuk mencegah looping ketika menggunakan topologi redundant switches. Contohnya jika kita menggunakan 3 switches yang saling terkoneksi dalam sebuah topology (seperti gambar di bawah ini), tanpa menggunakan STP maka akan timbul beberapa masalah seperti broadcast storm dimana frame selalu berputar di switch - switch tersebut tanpa henti. Jika masalah tersebut dibiarkan, ketika broadcast message yang dikirimkan oleh switch terus bertambah, maka perputaran frame - frame tersebut akan semakin banyak dan mengakibatkan jaringan jadi semakin lambat.

![topology stp](stp-topology-1.png)

Beberapa masalah yang timbul jika stp tidak diterapkan pada switch redundancy topology:
 
- Broadcast Storm

Terjadinya looping frame tanpa henti yang mengakibatkan jaringan menjadi semakin lambat dan beban pada switch juga semakin berat, frame looping tersebut akan berhenti jika salah satu switch down atau salah satu kabel terputus/tercabut. 

 - Unstable Mac Address Table

Mac address table yang tidak stabil atau tidak konsisten karena terjadinya frame looping, yang mengakibatkan 2 port yang berbeda di sebuah switch menerima mac address yang sama sehingga mac table nya akan selalu melakukan update terus menerus.

- Duplicated Frame

Frame yang sama akan terkirim berulang ulang. Contohnya ketika pc0 akan mengirimkan sebuah data ke pc2, jika switch0 belum tau lokasi dari pc2 mac address, maka switch0 akan melakukan flooding ke semua port kecuali port asal. Saat frame telah sampai ke switch2, maka data akan dikirimkan ke pc2. Tapi, switch1 juga menerima data yang dikirimkan oleh switch0, maka switch1 mengirimkannya lagi ke switch2 dan data yang diterima oleh switch2 akan diteruskan lagi ke pc2. Oleh karena itu, pc2 akan menerima data yang sama.

## Tipe - tipe STP

**STP / 802.1D**

STP standard untuk mencegah looping (STP original). Dalam tipe ini, STP dapat melakukan konfigurasi secara otomatis dan memberikan convergence yang lambat.

**PVST+ (Cisco Proprietary)**

Peningkatan dari STP standard yang dikembangkan oleh cisco dan penambahan fitur seperti konfigurasi STP pada setiap VLAN.

**RSTP / 802.1w**

Peningkatan dari STP standard dengan convergence yang jauh lebih cepat dan juga masih kompatible dengan STP standard

**Rapid PVST+ (Cisco Proprietary)**

Peningkatan dari RSTP yang dikembangkan oleh cisco dan penambahan fitur seperti konfigurasi RSTP pada setiap VLAN.

## Konfigurasi STP

Langkah langkah konfigurasi Spanning Tree:

1) Menentukan Root Bridge.
2) Mengubah Root interfaces menjadi forwading state.
3) Setiap non-root Switch menentukan interface mana yang akan menjadi Root Port pada masing masing switch.
4) Interface yang tersisa akan dipilih yang mana yang akan menjadi designated port.
5) Semua port yang tersisa akan dipilih menjadi blocking state.


![topology stp](stp-topology-name-2.png)

### Roles

- **Root Port** adalah port terbaik untuk mencapai Root Bridge.

- **Designated Port** yaitu port dengan ruter terbaik menuju Root Bridge.

- **Non-Designated Port** Port yang menjadi blocking state.

### States

- **Disabled** yaitu sebuah port dalam keadaan mati (shutdown).

- **Blocking** sebuah port yang mem-blok trafik

- **Listening** tidak men-forward trafik dan tidak mempelajari mac address.

- **Learning** tidak men-forward trafik tapi mempelajari mac address.

- **Forwarding** mengirim dan menerima trafik seperti biasa.

## Proses Spanning Tree

Setiap switch akan mempunyai sebuah BPDU (Bridge Protocol Data Unit). Dalam BPDU terdapat Root Cost, yang mana akan menjadi cost dari Root Bridge, Local BID dan Root BID. BID atau Bridge ID adalah faktor utama yang akan menentukan si Root Bridge.

Contoh BPDU:

	Root Cost: 0
	My BID: 32769aaaa:aaaa:aaaa
	Root BID: 32769aaaa:aaaa:aaaa

3279 merupakan value dari STP Priority, secara default STP Priority memiliki value 32768 ditambah dengan angka vlan (contoh vlan 1). Yang kedua adalah mac address. Simpelnya, switch dengan BID keseluruhan yang paling rendah yang akan menjadi Root Bridge.

## Konfigurasi Spanning Tree

1. **Menentukan Root Bridge**

Kita lihat dulu priority dan mac address nya.
Cara melihat priority dan mac address dari setiap switch adalah dengan menjalankan perintah **sh version** dan cari pada baris **Base ethernet MAC Address**, atau bisa juga menggunakan perintah **show spanning-tree** dan kita lihat pada bagian bridge id dan kita lihat pada bagian address dan priority-nya.

	switch0#sh spanning-tree 
	VLAN0001
	  Spanning tree enabled protocol ieee
	  Root ID    Priority    32769
	             Address     0002.173C.0027
	             Cost        19
	             Port        1(FastEthernet0/1)
	             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
	
	  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
	             Address     000A.4143.2445
	             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
	             Aging Time  20
	
	Interface        Role Sts Cost      Prio.Nbr Type
	---------------- ---- --- --------- -------- --------------------------------
	Fa0/2            Altn BLK 19        128.2    P2p
	Fa0/3            Desg FWD 19        128.3    P2p
	Fa0/1            Root FWD 19        128.1    P2p
<p/>

	switch1#sh spanning-tree
	VLAN0001
	  Spanning tree enabled protocol ieee
	  Root ID    Priority    32769
	             Address     0002.173C.0027
	             This bridge is the root
	             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
	
	  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
	             Address     0002.173C.0027
	             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
	             Aging Time  20
	
	Interface        Role Sts Cost      Prio.Nbr Type
	---------------- ---- --- --------- -------- --------------------------------
	Fa0/1            Desg FWD 19        128.1    P2p
	Fa0/2            Desg FWD 19        128.2    P2p
	Fa0/3            Desg FWD 19        128.3    P2p

<p/>

	switch2#show spanning-tree 
	VLAN0001
	  Spanning tree enabled protocol ieee
	  Root ID    Priority    32769
	             Address     0002.173C.0027
	             Cost        19
	             Port        1(FastEthernet0/1)
	             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
	
	  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
	             Address     0009.7C2A.82E9
	             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
	             Aging Time  20
	
	Interface        Role Sts Cost      Prio.Nbr Type
	---------------- ---- --- --------- -------- --------------------------------
	Fa0/1            Root FWD 19        128.1    P2p
	Fa0/2            Desg FWD 19        128.2    P2p
	Fa0/3            Desg FWD 19        128.3    P2p


| Hostname | Priority | MAC Address |
|---|---|---|
| Switch 0 | 32769 | 000A.4143.2445 |
| Switch 1 | 32769 | 0002.173C.0027 |
| Switch 2 | 32769 | 0009.7C2A.82E9 |

Karena priority nya sama semua, maka yang diliat selanjutnya mac-address yang paling rendah. Cara menghitung mac address yang paling kecil adalah dengan membadingkan oktet dari kiri ke kanan. Jika oktet pertama sama, maka kita hitung oktet selanjutnya dan seterusnya sampai kita menemukan perbedaan. Ketika kita membadingkan nilai hexadecimal, ingat jika "00" adalah nilai terkecil dan "FF" adalah nilai terbesar

Dalam kasus kita sekarang, kita akan membandingkan mac address dari switch0, switch1, dan switch2. Jika kita lihat, oktet pertama dari setiap switch memiliki value yang berbeda semua, dan switch1 memiliki nilai hexadecimal yang paling kecil nilainya. Maka switch1 lah yang akan menjadi Root Bridge.

2. **Menentukan Root Port**

Setiap switch dalam jaringan akan memiliki satu Root Port yang mengarah ke Root Bridge. Root Port dapat ditentukan oleh port costs pada sebuah interface.

| Port Speed | Original | New |
|---|---|---|
| 10Mbps | 100 | 2.000.000 |
| 100Mbps | 19 | 200.000 |
| 1Gbps | 4 | 20.000 |
| 10Gbps | 2 | 2.000 |
| 100Gbps | N/A | 200 |
| 1Tbps | N/A | 20 |

![topology stp portcost](stp-topology-name-portcost-3.png)

Port cost yang terendah akan dijadikan Root Port, sisanya akan dijadikan block port dan designated port. Tetapi, apa jadinya jika setiap interface mempunyai port cost yang sama? yang pertama akan dicari bid neighbor yang terkecil, jika masih sama, akan dicari neighbor port priority yang terendah, dan jika masih sama lagi, akan dicari neighbor port number yang terendah.

Setelah mencari Root Port, selanjutnya mencari yang mana yang akan dijadikan designated port. Cara mencarinya hampir sama dengan yang kita lakukan tadi yaitu mencari Root Port.

Langkah terakhir setelah mencari Root Port dan Designated Port, yaitu menentukan Blocking State. Setiap port yang bukan Root Port atau Designated Port maka akan dijadikan sebagai Blocking Port.

### Timers - Default

- **[Hello - 2 seconds]** -> waktu interval dimana Root Bridge akan mengirim hello messages, menginfokan ke setiap switch tau jika semua switch masih berfungsi normal
- **[MaxAge - 10 x Hello (20 seconds)]** -> waktu dimana switch akan menunggu dan sadar jika ada error.
- **[Forward Delay - 15 seconds]** -> waktu diantara listening dan learning state

### STP States

- **Disabled** -> Di state ini, keadaan port dalam keadaan mati dan tidak berpartisipasi apapun di STP.
- **Blocking** -> Di state ini, port tidak men-forward frames tapi masih ikut andil dalam STP. Port di keadaan ini masih melakukan pengamatan terhadap BPDU untuk mengumpulkan informasi dalam topology jaringan. Blocking state memerlukan 20 detik secara default untuk transisi ke Listening state
- **Listening** -> Port dalam keadaan listening state mulai men-forward BPDU dan melakukan persiapan untuk kemungkinan transisi ke learning state. State ini memerlukan waktu 15 detik secara default untuk memastikan transisi berjalan dengan lancar
- **Learning** -> Port melanjutkan partisipasinya dengan mulai mempelajari mac address dengan menginspeksi packet yang melewatinya. Port ini belum men-formward frames. Secara default, dalam state ini secara default memerlukan waktu 15 detik untuk membuat mac address table.
- **Forwarding** -> Dalam state inilah dimana port beroperasi sepenuhnya. Port dalam keadaan ini mulai melakukan forward frames. Waktu transisi ke Forwarding state secara default membutuhkan waktu sekitar 15 detik.
- **Broken/Disabled** -> State ini merujuk pada port yang sudah bukan lagi bagian dari jaringan dikarenakan kesalahan. State ini mengubah kembali port menjadi blocking port

## Case Example

![case image](stp-topology-name-case-4.png)

Contoh kasus, anggap link yang terhubung antara switch 1 dan switch 2 error, maka switch 2 akan berhenti mendapatkan hello message dari switch 1. Pertama - tama switch 0 akan menunggu max-age timer yaitu 20 detik. Kemudian ketika switch 0 sadar jika ada yang salah, switch 0 akan bereaksi dengan mengubah blocking port yang ada di switch 0 menjadi Designated Port, dan Designated Port yang ada di switch 2 akan diubah menjadi Root Port.

Disinilah permasalahannya, Block Port tidak bisa transisi ke forwarding state secara langsung, terdapat delay dari state lain dalam masa transisi. Pertama, port akan ber-transisi ke Listening state selama 15 detik, kedua, Learning state 15 detik, setelah itu baru port berubah ke forwarding state. Dan ini membutuhkan total sekitar 50 detik dalam masa transisinya.

Permasalahan tersebut juga bukan hanya untuk port yang terkoneksi ke switch. Kondisi ini juga terjadi antara koneksi switch ke PC. Ketika kita hubungkan PC ke switch, port yang terhubung akan tetap melewati listening dan learning state terlebih dahulu. Untuk mempercepat convergence tersebut kita bisa menggunakan portfast dan BPDU guard, tapi solusi tersebut hanya berlaku untuk switch port yang terhubung ke komputer.

Solusi untuk mempercepat convergence antar port switch adalah dengan menggunakan protokol yang lebih baru dan lebih cepat yaitu Rapid Spanning Tree Protocol (RSTP), jauh lebih cepat dari pada standard STP. Untuk RSTP kita akan bahas di lain artikel.
