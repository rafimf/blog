---
title: About
description: Blog Seputar Networking
date: '2019-02-28'
aliases:
  - about-us
  - about-hugo
  - contact
license: CC BY-NC-ND
lastmod: '2020-10-09'
menu:
    main: 
        weight: -90
        params:
            icon: user
---

Blog dibuat sebagai sarana berbagi pengetahuan tentang networking dan pengetahuan IT secara umum. Website ini dibuat menggunakan [Static Site Generator Hugo](gohugo.io) menggunakan tema [Stack](https://themes.gohugo.io/themes/hugo-theme-stack/). Yang pengen tau siapa saya bisa mampir ke link berikut [rafimf.gitlab.io](https://rafimf.gitlab.io)

